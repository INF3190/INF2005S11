<?

//lecture du fichier json
$jsonfile="liste.json";
$jsondata=file_get_contents("$jsonfile");

echo "<pre>";
print_r($jsondata);
echo "</pre>";
echo "<hr>";
//decodage de donnees json en tableau php
$tab=json_decode($jsondata,JSON_UNESCAPED_UNICODE);
echo "<pre>";
print_r($tab);
echo "</pre>";
$pers=new stdClass;
$pers->nom="Marion";
$pers->prenom="Francine";
$pers->age=23;
//ajout d'un élément au tableau
$tab[]=$pers;
echo "<hr>";
echo "<pre>";
print_r($tab);
echo "</pre>";
//convertir tableau php en json
$tab2json=json_encode($tab);
echo "<hr>";
echo "<pre>";
print_r($tab2json);
echo "</pre>";
//enregistrer le contenu dans le fichier
file_put_contents("$jsonfile", $tab2json);

?>