<?php
require 'head.php';

if(!isset($_REQUEST["soumettre"])){
    // isset veérifie si une variable est initialisée
   die("<span style='color:red;'>Erreur: Aucun formulaire soumis</span>"); 
}
echo "<h3>Information université et ville</h3>";

if(isset($_REQUEST['univ'])){// issue de bouton radio
    echo "<span class='h5'>Université: ". strtoupper($_REQUEST['univ'])."</span><br/>";
    // strtoupper($chaine) mets $chaine en majuscule.
    // strtolower($chaine) mets %chaine  minuscule.
}
//echo json_encode($_REQUEST);

if(isset($_REQUEST['villes'])){
    echo "<span class='h5'>Villes sélectionnées :</span>";
    $villes= $_REQUEST['villes']; //un tebleau des checkboxes (cases à cocher)
    echo "<ul>";
    foreach ($villes as $ville){
        $ville= trim($ville); // trim enleve espace blanc autour de la chaine
        $ville= strtoupper($ville);
        echo "<li>$ville</li>";
    }
    echo "</ul>";
}
echo "<hr>";
echo "<h3>Information Fichier</h3>";
if(isset($_FILES["fichier"])){
    // "fichier" est le nom du champ fichier dan le formulaire
    $infoFich = $_FILES["fichier"];
    $fileName = $infoFich['name']; // nom du fichier chargé par l,utilisateur
    $tmpFile=$infoFich['tmp_name'];// fichier temporaire après téléversement
    $saveDir="images";
    $savedFile="$saveDir"."/$fileName";
    echo "<dl class='dl-horizontal'>";
    echo "<dt>Nom: </dt>";
    echo "<dd>".$infoFich['name']."</dd>";
    //------
    echo "<dt>Type: </dt>";
    echo "<dd>".$infoFich['type']."</dd>";
    //------
    echo "<dt>Fichier temporaire: </dt>";
    echo "<dd>".$infoFich['tmp_name']."</dd>";
    //------
    echo "<dt>Emplace d'enregistrement: </dt>";
    echo "<dd>".$savedFile."</dd>";
    //------
    echo "<dt>Taille: </dt>";
    echo "<dd>".$infoFich['size']."</dd>";
    //------
    echo "<dt>Erreur: </dt>";
    echo "<dd>".$infoFich['error']."</dd>";
    
    echo "</dl>";
    //enregistrement du fichier
    if(!file_exists($saveDir)){
        //si le dossier n'existe pas
        mkdir("$saveDir")||die("<span style='color:red'>impossible de créer le dossier $saveDir</span>");
        // Au besoins donner les permission en écriture dans le dossier : chmod o+w NOM_DOSSIER
        // NOM_DOSSIER doit correspondre au non du dossier dans lequel on doit créer le sous dossier pour enregistrer
    }
    
    move_uploaded_file("$tmpFile","$savedFile")||die("<span style='color:red'>impossible de saugarder $fileName  dans $saveDir</span>");
    //move_uploaded_file($old,$new) enregistrer le fichier televersé d,un dossier à l'autre 
   //affichage image charge
   echo "<img src='".$savedFile."' class='img img-circle center-block'  style='width:200px;' alt='image téléversée'>";
}
//echo print_r($_FILES);
    //move_uploaded_file($tmp_name, "$uploads_dir/$name");
require 'tail.php';